#! /bin/sh -e

[[ ! -d "$MOUNTPOINT" ]] && mkdir -p "$MOUNTPOINT"

if [[ -z $NFS_SERVER ]]; then
rpc.statd & rpcbind -f & echo "docker NFS client with rpcbind ENABLED... if you wish to mount the mountpoint in this container USE THE FOLLOWING SYNTAX instead:
$ docker run -itd --privileged=true --net=host -v vol:/mnt/nfs-1:shared -e NFS_SERVER= X.X.X.X -e NFS_PATH=shared_path d3fk/nfs-client" & more
else
  MOUNT_OPTIONS_CMD=" "
  if [[ ! -z $MOUNT_OPTIONS ]]; then
    MOUNT_OPTIONS_CMD=" -o '$MOUNT_OPTIONS' "
  fi
  MOUNT_CMD="mount -t '$FSTYPE'${MOUNT_OPTIONS_CMD}'$NFS_SERVER:$NFS_PATH' '$MOUNTPOINT'"
  echo "MOUNT_CMD : ${MOUNT_CMD}"
  rpc.statd & rpcbind -f &
    eval "${MOUNT_CMD}"
fi
mount | grep nfs

while true; do sleep 1000; done

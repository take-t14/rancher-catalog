#!/bin/bash

curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
helm repo add jetstack https://charts.jetstack.io  
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest  
helm upgrade --install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.9.1 --set installCRDs=true  

sleep 30

helm upgrade --install rancher rancher-latest/rancher --version 2.6.7 --namespace cattle-system --create-namespace --set hostname=local-rancher.d-a.co.jp --set replicas=1 --set ingress.tls.source=secret --wait --timeout=10m  
kubectl -n cattle-system create secret tls tls-rancher-ingress --cert=./key/server.crt --key=./key/server.key  

sleep 30

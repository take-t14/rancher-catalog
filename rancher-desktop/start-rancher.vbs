Option Explicit

Private Sub Main()
  Dim strPROC_NAME
    
  '実行ファイル(プロセス名)を代入
  strPROC_NAME = "%LOCALAPPDATA%\Programs\Rancher Desktop\Rancher Desktop.exe"
  '二重起動防止
  If fncGet_ProcStatus(strPROC_NAME)=True Then
    MsgBox "既に起動中です。", 64, "Rancher Desktop"
    '二重起動の場合、処理終了
    Exit Sub
  End If
  '処理を続行
  Dim objWshShell
  Set objWshShell = WScript.CreateObject("WScript.Shell")
  Call objWshShell.Run("""" & strPROC_NAME & """", 1, True)
  Set objWshShell = Nothing
End Sub

'/------------------------------------------------
'  関数名   :   fncGet_ProcStatus
'  機能概要 :   引数で指定したプロセス名の起動を確認する。
'  引数     :   strProcName(String) : 起動状態を確認するプロセス名
'  戻り値   :   Boolean, True=起動していない,False=起動している
'------------------------------------------------/
Private Function fncGet_ProcStatus(ByVal strProcName)
  Dim Service, ServiceSet, lngCounta, strProdFN, objFS
  fncGet_ProcStatus = False
  lngCounta = 0
  Set objFS = CreateObject("Scripting.FileSystemObject")
  strProdFN = objFS.getFileName(strProcName)
  Set objFS = Nothing
  'WMI(WindowsManagementInstrumentation)オブジェクトの宣言
  Set ServiceSet = GetObject("winmgmts:{impersonationLevel=impersonate}"). _
                    InstancesOf("Win32_Process")
  For Each Service In ServiceSet
    If Service.Description = strProdFN Then
      lngCounta = lngCounta + 1
    End If
  Next

  If 1 <= lngCounta Then
    '起動しているプロセスが１つ以上ならTrue
    fncGet_ProcStatus = True
  End If
    
  'WMIオブジェクトの解放
  If Not (ServiceSet Is Nothing) Then
    Set ServiceSet = Nothing
  End If
  Exit Function
End Function

Call Main()
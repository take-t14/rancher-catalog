#!/bin/bash

APP_DATA=$1
APP_DATA=/mnt/`echo "${APP_DATA}" | sed -e 's|\\\\|\/|g' | sed -e "s|:||g" | sed -e "s|^C\/|c\/|"`
INSECURE_REG_FILE="${APP_DATA}/rancher-desktop/provisioning/insecure-registry.start"

cat << EOF > ${INSECURE_REG_FILE}
#!/bin/sh

cp -a /etc/containerd/config.toml.tmpl /etc/containerd/config.toml
service containerd restart
EOF

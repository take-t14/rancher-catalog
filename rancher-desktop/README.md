__**************************************************************************************__  
__*　Rancher Desktop におけるLAPP環境構築__  
__**************************************************************************************__  
  
__**************************************************************************************__  
__*　前提条件：この設定ファイルの環境要件__  
__**************************************************************************************__  
  
【環境要件】  
◆OS  
・Windows10 Pro(x64)  
  
◆ソフトウェア  
・Rancher Desktop 1.11.1  
・Ubuntu 22.04 LTS(WSL2)  
・Kubernetes v1.26.12  
・rancher（helm） 2.7.9  
  
__**************************************************************************************__  
__*　＜１＞ubernetesを動かす基盤となるソフトウェアのインストール（全てUbuntu 22.04 LTSで実施）__  
__*　※ 1回だけ実施すればよい。__  
__**************************************************************************************__  

#### # WSLのインストールをする
  
※Windows PowerShellを起動（非管理者権限）して以下を実行＆PC再起動  
wsl --list --online  
※ここでリストアップされたディスとリビュージョンを、下記の-dオプションで指定する。  
wsl --install -d Ubuntu-22.04  
  
※WindowsストアアプリでUbuntu 22.04LTSをインストールして起動。ユーザ名とパスワードを入力する（作業PCと同じものを入力）。 
  
#### # WSLの設定をする（コマンドプロンプトで実行）  
  
※WSL2になっているかを確認＆なっていなければ切替  
以下でWSLのバージョンを確認する。  
C:\Users\user> wsl -l -v  
```  
  NAME                   STATE           VERSION
* Ubuntu-22.04           Running         2
```  
VERSIONが1になっている場合は以下コマンドで2にする。
「Ubuntu-22.04」は上記で表示されるNAMEに合わせて下さい。
```  
C:\Users\user> wsl --set-default-version 2  
C:\Users\user> wsl --set-version Ubuntu-22.04 2  
```  
  
※「wsl --set-version Ubuntu-22.04 2」を実行した際に下記のエラーが出たら、https://aka.ms/wsl2kernelからカーネルをDLしてインストールして再試行する。  
```  
WSL 2 を実行するには、カーネル コンポーネントの更新が必要です。詳細については https://aka.ms/wsl2kernel を参照してください  
```  
  
設定が完了したら以下コマンドをコマンドプロンプトで実行。  
```  
C:\Users\user> wsl --shutdown
```  
以下コマンドをWSLで実行し、「5.10.16.3-microsoft-standard-WSL2」と出れば設定完了。
```  
user@wsl:~$ uname -r
5.10.16.3-microsoft-standard-WSL2
```  
  
※Windowsボタン＋Ctrlで開く「ファイル名を指定して実行」で「%USERPROFILE%」と入力してエンター押下。開いたWindowsエクスプローラーで「.wslconfig」というファイルを作成し、以下の内容を記載する。  
```  
[wsl2]
processors=2
memory=3500MB
swap=0
```  
  
  
#### # gitの設定  
user@wsl:~$ ssh-keygen -t rsa  
  
※パスフレーズは未入力でエンター押下  
  
user@wsl:~$ cat ~/.ssh/id_rsa.pub  
  
※出力結果をGitLab → 右上のプロフィールアイコン → 設定 → SSH鍵 → Keyへ追加する  
  
設定が完了したら以下コマンドをコマンドプロンプトで実行。  
```  
C:\Users\user> wsl --shutdown
```   
  
#### # rancher-catalogsのGitLabのプロジェクトを「C:\k8s\rancher-catalogs」へgit cloneする（WSLで実行）  
user@wsl:~$ mkdir -p /mnt/c/k8s/rancher-catalogs  
user@wsl:~$ cd /mnt/c/k8s/rancher-catalogs  
user@wsl:~$ git clone git@xxxxxxxx/rancher-catalogs.git .  
  
※移行の手順はrancher-catalogsをVSCodeで開いて、/mnt/c/k8s/rancher-catalogs/rancher-desktop/README.mdを  
　VSCodeで見ながら実施するとやりやすいです。  
  
  
__**************************************************************************************__  
__*　＜２＞Rancher Desktopインストール・セットアップ__  
__*　※ 1回だけ実施すれば良い。__  
__**************************************************************************************__  
  
__*******************************************__  
__*　Rancher Desktopインストール・設定__  
__*******************************************__  
  
#### # Rancher Desktopインストール  
＜インストール時の指定＞  
◆Kubernetes Version：v1.24.4  
◆Enable Traefik：チェックON  
◆File → Preferences → WSL → Ubuntu-22.04をチェックしてApply
◆File → Preferences → Application → Automatic UpdatesをチェックOFFにしてApply
  
＜コンテキストの選択＞  
タスクアイコンのRancher Desktopを右クリック → Kubernetes Contexts → rancher-desktopを選択  
  
＜Windowsエクスプローラ設定＞  
◆Windowsエクスプローラーからコンテナがマウントする永続ボリュームへ書き込み・参照したい時のパス  
Windowsキー＋Ｒキーで開く「ファイル名を指定して実行」ダイアログへ、「\\wsl$」を貼り付けてエンター。  
「rancher-desktop」を右クリックし、「ネットワークドライブの割り当て」をクリックしてドライブレターを選択して「完了」を押下する。  
マウントしたドライブレターが「Z」の場合は、Windowsキー＋Pause → システムの詳細設定 → 環境変数 → システムの環境変数  
 → 新規で、変数名 RDRIVE、変数値 Z:\ を設定する。  
  
※以下コマンドをWSLで実行  
```  
sudo mkdir -p /mnt/z
sudo mount -t drvfs Z: /mnt/z
```  
  
※Rancher Desktopが起動状態でないと「\\wsl$\rancher-desktop」は出現しません。  
  
※よくある間違い  
「\\wsl$\Ubuntu-22.04」ではありません。「\\wsl$\rancher-desktop」をZドライブとしてマウントして下さい。  
  
＜Rancher Desktopの起動＞  
Rancher Desktopは多重起動防止が無く、実行したら実行した数分起動します。ところがその状態になると  
Rancher Desktopは障害状態となり使えなくなります。rancher-desktop\start-rancher.vbsを実行して  
Rancher Desktopを起動すると、多重起動防止を実装しているため既に起動済みの場合は起動をスキップします。  
こちらを使ってRancher Desktopを起動願います。  
  
__**************************************************************************************__  
__*　＜３＞Rancher Desktop設定__  
__*　※ 1回だけ実施すれば良い。__  
__*　以下「【１】スクリプトで実行する場合」または、__  
__*　「【２】手動で実行する場合」のどちらかの手順で設定する。__  
__**************************************************************************************__  

__*******************************************__  
__*　【１】スクリプトで実行する場合__  
__*******************************************__  
  
#### # １．1.build-rancher.batをダブルクリックして実行  
「1.build-rancher.bat」をダブルクリックして立ち上がったコマンドプロンプトが終了するまで待機する事。  
  
※下記のワーニングが出るが無視して問題ない  
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/[ユーザ名]/.kube/config  
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/[ユーザ名]/.kube/config  
  
  
#### # ２．Rancher Desktop再起動  
タスクアイコンのRancher Desktopを右クリックし、「Quit Rancher Desktop」をクリック。  
rancher-desktop\start-rancher.vbsを実行。  
  
※Rancher DesktopのUIのプログレスバー表示がなくなるまで待機  
  
#### # ３．2.db-data-deploy.batをダブルクリックして実行  
※2.db-data-deploy.batをダブルクリックして立ち上がったコマンドプロンプトが終了するまで待機する事。  
  
#### # ４．以下「＜４＞Rancher共通設定」の手順を実施する  
  
※＜４＞Rancher共通設定の手順実施後は、以下「＜５＞動作確認」へ進む。（以下「【２】手動で実行する場合」は実施不要）  
  
  
__*******************************************__  
__*　【２】手動で実行する場合__  
__*******************************************__  
  
#### # １．Rancher Desktopの初期設定（WSLで実行）  
  
##### # １－１．kubectl、helmコマンドをWSLへインストール  
user@wsl:~$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -   
user@wsl:~$ echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list  
user@wsl:~$ sudo apt-get update && sudo apt-get install -y kubelet kubeadm kubectl kubernetes-cni  
user@wsl:~$ sudo curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
user@wsl:~$ helm repo add jetstack https://charts.jetstack.io  
user@wsl:~$ helm repo add rancher-latest https://releases.rancher.com/server-charts/latest  
user@wsl:~$ helm search repo rancher-stable/rancher --versions  
  
##### # １－２．rancherのUIのデプロイ  
user@wsl:~$ kubectl create namespace cert-manager  
user@wsl:~$ helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.13.3 --set installCRDs=true  
user@wsl:~$ kubectl create namespace cattle-system  
user@wsl:~$ helm install rancher rancher-latest/rancher --version 2.7.9 --namespace cattle-system --set hostname=rancher.rd.localhost --set replicas=1 --set ingress.tls.source=secret --wait --timeout=10m  
user@wsl:~$ kubectl get nodes; kubectl get pods -o wide --all-namespaces  
  
##### # ２－５．Rancher Desktop再起動  
タスクアイコンのRancher Desktopを右クリックし、「Quit Rancher Desktop」をクリック。  
rancher-desktop\start-rancher.vbsを実行。  
  
#### # ３．以下「＜４＞Rancher共通設定」の手順を実施する  
  
※＜４＞Rancher共通設定の手順実施後は、以下４．Project、Namespace作成へ進む。  
  
#### # ４．Project、Namespace作成  
Project/Namespace → Create Projectでプロジェクト作成。  
プロジェクト右上のCreate Namespaceでネームスペース作成。  

##### # ４－１．developプロジェクト作成  
◆プロジェクト名：develop  
◆ネームスペース：dev-apache-php、dev-mysql、dev-pgpool-postgresql  
  
◆プロジェクト名：common-systems  
◆ネームスペース：common-systems  
  
  
#### # ５．Appインストール  
App&Marketplace → ChartsでインストールするAppを押下して、Appをインストールする。  
  
##### # ５－１．dev-apache-php  
◆version：0.0.89  
◆Namespace：dev-apache-php  
◆Name：dev-apache-php  

affinity: {}
initConfig:
  - branch: master
    phpVersion: 83
persistence:
  storageClass: hostpath

##### # ５－２．Pgpool-II-PostgreSQL  
◆version：0.0.71
◆Namespace：dev-pgpool-postgresql  
◆Name：dev-pgpool-postgresql  
  
◆Values：  
※以下の項目箇所のみを、以下の内容で置き換える  
```  
affinity: {}
data:
  pgpoolConf: |-
    listen_addresses = '*'
    port = 9999
    socket_dir = '/var/run/pgpool'
    pcp_listen_addresses = '*'
    pcp_port = 30002
    pcp_socket_dir = '/var/run/pgpool'
    backend_hostname0 = 'postgresql'
    backend_port0 = 5432
    backend_weight0 = 1
    backend_flag0 = 'ALWAYS_PRIMARY|DISALLOW_TO_FAILOVER'
    backend_hostname1 = 'postgresql2'
    backend_port1 = 5432
    backend_weight1 = 1
    backend_flag1 = 'DISALLOW_TO_FAILOVER'
    sr_check_period = 0
    enable_pool_hba = on
    backend_clustering_mode = 'streaming_replication'
    num_init_children = 32
    max_pool = 4
    child_life_time = 300
    child_max_connections = 0
    connection_life_time = 0
    client_idle_limit = 0
    connection_cache = on
    load_balance_mode = on
    ssl = on
    failover_on_backend_error = off
  poolHbaConf: |-
    local   all         all                               trust
    host    all         all         127.0.0.1/32          trust
    host    all         all         ::1/128               trust
    host    all         all         0.0.0.0/0             md5
env: 
  POSTGRES_REPLICATION_MODE: master
  POSTGRESQL_HOT_STANDBY: 'off'
global: 
  pgpool: 
    pgpoolDatabase: postgres
  postgresql: 
    postgresqlDatabase: postgres
persistence:  
  storageClass: hostpath
```  
  
※【！注！よくある間違い】  
env:配下の値を例に説明します。  
env:配下を全部消して上記POSTGRES_REPLICATION_MODE:とPOSTGRESQL_HOT_STANDBY:だけ登録するのは間違いです。  
env:配下の他の値はノータッチで、POSTGRES_REPLICATION_MODE:とPOSTGRESQL_HOT_STANDBY:だけ上記の値にして下さい。  
  
※Workload → Pod → dev-pgpool-postgresql-statefulset-0のpgpoolとpgpool-statsが×の場合  
Workload → Pod → dev-pgpool-postgresql-statefulset-0の右の・・・ → Deleteを実行し、回復しないかを確認。  
  
##### # ５－３．mysql  
◆version：0.0.30  
◆Namespace：dev-mysql  
◆Name：dev-mysql  
  
◆Values：  
※以下の項目箇所のみを、以下の内容で置き換える  
```  
affinity: {}
env: 
  mysql_replication_mode: master
master:
  initConfConfigMap:
    debug.cnf: |
      [mysqld]
      general_log=1
resources:
  requests:
    cpu: 500m
```  
  
※【！注！よくある間違い】  
env:配下の値を例に説明します。  
env:配下を全部消して上記mysql_replication_mode:だけ登録するのは間違いです。  
env:配下の他の値はノータッチで、mysql_replication_mode:だけ上記の値にして下さい。  
  
  
##### # ５－４．postfix  
◆version：1.0.11  
◆Namespace：common-systems  
◆Name：postfix-sv  
  
  
__**************************************************************************************__  
__*　＜４＞Rancher共通設定__  
__*　※ 1回だけ実施すれば良い。__  
__**************************************************************************************__  
  
##### # １－２．WSLで仮パスワード確認（WSLで実行）  
user@wsl:~$ kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}{{"\n"}}'  
  
※kubectlがCommand Not Foundとなる場合は、WSLを再起動して改善するかどうかを確認する。  
  
##### # １－３．Rancher Desktopへブラウザでアクセス  
https://rancher.rd.localhost/  
  
##### # １－４．パスワード入力欄に１－２で確認したパスワードを入力してログイン  

##### # １－５．画面に表示された自動生成パスワードをメモする  
「Set a specific password to use: 」ラジオボタンを選択し、下記のパスワードを入力する。  
  
1CaGwnVEKptT63Fh  
  
（※）次回以降ログインする際は、Username：「admin」、Passwordにはメモしたパスワードを入力してログインする  
（※）ログイン後、「local」リンクを押下してダッシュボードを表示する。  
  
  
#### # ４．Rancherカタログ登録  
App&Marketplace → Registries → Createを押下して、Rancherカタログを登録する。  
  
##### # ４－１．rancher-catalog  
◆Name : rancher-catalog  
◆Target : git  
◆Git Repo URL : https://gitlab.com/take-t14/rancher-catalog.git  
◆Git Branch : main  
  
__*******************************************__  
__*　各種設定ノウハウ__  
__*******************************************__  
  
#### # コンテナへ接続  　
user@wsl:~$ kubectl get nodes; kubectl get pods -o wide --all-namespaces  
user@wsl:~$ kubectl exec -it dev-apache-php-6498795c6c-xsgh9 -n dev-apache-php  --container php83-fpm -- /bin/bash  

#### # RancherDesktopリセット手順  
１．タスクアイコンのRancherDesktop右クリック → Preference → Cancel → Troubleshooting → Factory Resetを押下  
  
２．「Welcome to Rancher Desktop」と表示された画面が出たら、上記「１．インストーラーをダブルクリックしてインストール」  
　　の＜インストール時の指定＞と、＜コンテキストの選択＞の設定を実施。  
  
３．タスクアイコンのRancherDesktop右クリック → Quit Rancher Desktopをクリック。  
  
４．コマンドプロンプトで以下コマンドを実行  
C:\Users\user> wsl --shutdown  
  
５．Ubuntu 22.04LTSを起動  

６．Rancher Desktopを起動  
  
７．上記「２－２．rancherのUIのデプロイ」～「８－２．MySQL」の手順を再実施する。  
  
  
##### # PostgreSQL  
postgresql-portの右のFowardボタンを押下し、チェックボタンを押下。  
ホストをlocalhost、ポートを上記画面で出てきたポート番号を指定してDB接続する。  
  
※「Foward」ボタンを押下時に自動でランダムのポート番号が表示されますが、任意のポートを指定する事も可。  
  
##### # MySQL  
mysqlの右のFowardボタンを押下し、チェックボタンを押下。  
ホストをlocalhost、ポートを上記画面で出てきたポート番号を指定してDB接続する。  
  
※「Foward」ボタンを押下時に自動でランダムのポート番号が表示されますが、任意のポートを指定する事も可。  
  
#### # Appのアップデート手順  
１．Apps→Chartsをクリック  
２．右上の更新ボタンを押下。（更新ボタンが緑のチェックマークになるまで待つ）  
３．右上の更新ボタンを押下。（あえて2回実行）（更新ボタンが緑のチェックマークになるまで待つ）  
４．Apps→Installed AppsのアップデートしたいAppのUpgrableの「x.x.x↑」という黄色いアップグレードボタンをクリック  
５．Nextをクリック  
６．Upgradeをクリック  
  
  
#### # 永続ボリューム削除  
  
##### # dev-apache-phpの永続ボリューム削除（コマンドプロンプトで実行）  
C:\Users\user> wsl -d rancher-desktop -e sudo rm -rf /home/dev-apache-php  
  
##### # pgpool-postgresqlの永続ボリューム削除（コマンドプロンプトで実行）  
C:\k8s\rancher-catalogs\rancher-desktop\postgresql\force-delete.bat  
  
##### # mysqlの永続ボリューム削除（コマンドプロンプトで実行）  
C:\k8s\rancher-catalogs\rancher-desktop\mysql\force-delete.bat  

#### # WSLでWinMerge連携  
WLSで以下コマンドを実行する  
```   
user@wsl:~$ sudo ln -s /mnt/c/Program\ Files/WinMerge/WinMergeU.exe /usr/local/bin/windiff  
user@wsl:~$ vi ~/.gitconfig
※以下の内容を追記
[diff]
        tool = windiff
[difftool "windiff"]
        cmd = windiff -r \"$(wslpath -aw $LOCAL)\" \"$(wslpath -aw $REMOTE)\"
[merge]
        tool = windiff
[mergetool "windiff"]
        cmd = windiff -r --auto-merge \"$(wslpath -aw $LOCAL)\" \"$(wslpath -aw $BASE)\" \"$(wslpath -aw $REMOTE)\" --output \"$(wslpath -aw $MERGED)\" --label=Local --label=Base --label=Remote --diff \"$(wslpath -aw $BASE)\" \"$(wslpath -aw $LOCAL)\" --diff \"$(wslpath -aw $BASE)\" \"$(wslpath -aw $REMOTE)\"
[alias]
        windiff = difftool -y -d --no-symlinks -t windiff
        winmerge = mergetool -y -t windiff
```   
  
##### # Dockerインストール＆自動起動設定（Rancher Desktopの利用のみの場合は不要。dockerイメージビルドする場合に必要となる手順）  
※dockerのインストール  
da-hatakeyama@P10344:~$ curl https://get.docker.com | sh  
da-hatakeyama@P10344:~$ sudo apt install libsecret-1-0 gnupg2 pass  
  
※レガシー版のiptablesに切り替え  
da-hatakeyama@P10344:~$ sudo update-alternatives --set iptables /usr/sbin/iptables-legacy  
da-hatakeyama@P10344:~$ sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy  
  
da-hatakeyama@P10344:~$ sudo visudo  
  
##### # 以下をvisudoファイル末尾へ追記。[ユーザーID]には自分のユーザ名を入れること  
```
[ユーザーID] ALL=(ALL:ALL) NOPASSWD: /usr/sbin/service docker start
[ユーザーID] ALL=(ALL:ALL) NOPASSWD: /usr/sbin/service docker stop
```
  
da-hatakeyama@P10344:~$ sudo /usr/sbin/service docker start  
da-hatakeyama@P10344:~$ sudo gpasswd -a [自分のユーザ名] docker  
※以下でWSLを一度終了する  
da-hatakeyama@P10344:~$ exit  
※WSLを起動  
da-hatakeyama@P10344:~$ docker ps -a  
※エラーが出た場合は、以下「#### # docker ps -aでエラーが出た場合」を試してみる事。  
  
da-hatakeyama@P10344:~$ vi ~/.bashrc  
```  
ファイル末尾へ以下を追記
if [ $(service docker status | awk '{print $4}') = "not" ]; then
    sudo /usr/sbin/service docker start
fi
```  
#### # WSL2の容量を減らす対策  
  
※事前にZドライブを切断してPC再起動し、再起動後開いているエクスプローラーがあったら全て終了しておく。  
  
※タスクアイコンのRancher Desktopを右クリックし、「Quit Rancher Desktop」をクリック。  
※Windows PowerShellを管理者権限で起動  
  
PS C:\Users\da-hatakeyama> wsl --shutdown  
  
PS C:\Users\da-hatakeyama> diskpart  
  
※Diskpartのwindowに移動  
DISKPART> select vdisk file="C:\Users\[ユーザー名]\AppData\Local\Packages\CanonicalGroupLimited.Ubuntu22.04LTS_79rhkp1fndgsc（※１）\LocalState\ext4.vhdx"（※２）  
DISKPART> attach vdisk readonly（※３）  
DISKPART> compact vdisk  
DISKPART> detach vdisk  
DISKPART> exit  
  
（※１）「Ubuntu22.04LTS_79rhkp1fndgsc」の部分は環境によって異なる。「C:\Users\[ユーザー名]\AppData\Local\Packages」配下へ移動して確認する事。  
  
（※２）別途下記パスに対しても実施する。  
C:\Users\da-hatakeyama\AppData\Local\rancher-desktop\distro\ext4.vhdx  
C:\Users\da-hatakeyama\AppData\Local\rancher-desktop\distro-data\ext4.vhdx  
  
（※３）使用中のエラーが出た場合は、上に書いている下記を実行する。  
```  
※タスクアイコンのRancher Desktopを右クリックし、「Quit Rancher Desktop」をクリック。  
※Windows PowerShellを管理者権限で起動  
PS C:\Users\da-hatakeyama> wsl --shutdown  
※Zドライブを切断し、開いているエクスプローラーがあったら全て終了する。  
```  
  
※上の方に記載している「＜Windowsエクスプローラ設定＞」のネットワークドライブの割り当ての手順を実施する。  
※上記手順が完了したらPC再起動する。  
  
※参考サイト  
https://zenn.dev/anko/articles/976d904e53d87e  
  

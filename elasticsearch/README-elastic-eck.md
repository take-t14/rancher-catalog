# ＜１＞elasticsearch image build
tadanobu@Yukiko-HP:/$ docker login registry.gitlab.com
tadanobu@Yukiko-HP:/$ sudo /usr/sbin/service docker start; cd /mnt/c/k8s/rancher-catalog/mysql/Docker; docker build -t registry.gitlab.com/take-t14/rancher-catalog/mysql:latest .; docker push registry.gitlab.com/take-t14/rancher-catalog/mysql:latest
tadanobu@Yukiko-HP:Docker$ cd /mnt/c/k8s/rancher-catalog; git add .; git commit . -m "elasticsearch更新"; git push -u origin main

## １．Repositories
Name : rook-release
URL : https://charts.rook.io/release
Type : https

Name : elastic
URL : https://helm.elastic.co
Type : https

Name : lebenitza
URL : https://lebenitza.github.io/charts
Type : https


## ２．Appインストール
Name : rook-ceph
Namespace: rook-ceph

### eck operator
Name : eck-operator
Namespace: elastic-system

Name : eck-elasticsearch
Namespace: elastic-system
values:
annotations:
  eck.k8s.elastic.co/license: basic
nodeSets:
  - config:
      node.store.allow_mmap: false
      xpack.security.authc:
        anonymous:
          authz_exception: false
          roles: superuser
          username: anonymous
    podTemplate:
      spec:
        http:
          tls:
            selfSignedCertificate:
              disabled: true
    volumeClaimTemplates:
      - metadata:
          name: elasticsearch-data
        spec:
          accessModes:
            - ReadWriteOnce
          resources:
            requests:
              storage: 10Gi
          storageClassName: ceph-block

Name : eck-kibana
Namespace: elastic-system
values:
annotations:
  eck.k8s.elastic.co/license: basic
spec:
  elasticsearchRef:
    name: eck-elasticsearch
    namespace: elastic-system
  http:
    service:
      spec:
        type: LoadBalancer
        externalIPs:
        - 192.168.0.130
    tls:
      selfSignedCertificate:
        disabled: true

eck-elasticsearch-es-http.elastic-system.svc.cluster.local


### eck elasticsearch-logrotate
Name : elasticsearch-curator
Namespace: common-systems
values:

  action_file_yml: |-
    ---
    actions:
      1:
        filters:
        - filtertype: pattern
          kind: prefix
          value: idx-dev-apache-php-
        - filtertype: age
          source: name
          direction: older
          timestring: '%Y%m%d'
          unit: days
          unit_count: 1
          stats_result: min_value
          epoch:
          exclude: False
  config_yml: |-
    ---
    client:
      hosts:
        - elasticsearch-master.common-systems.svc.cluster.local
      use_ssl: False
      ssl_no_validate: True

image:
  pullPolicy: IfNotPresent
  repository: anjia0532/curator
  tag: 5.8.5
#  pullPolicy: IfNotPresent
#  repository: bitnami/elasticsearch-curator
#  tag: 5.8.4-debian-10-r253


## ９９．ElasticSearch登録テスト

### # eck-elasticsearch-es-http
curl -XPUT -k 'https://eck-elasticsearch-es-http.elastic-system.svc.cluster.local:9200/dev-apache-php/dev-apache-php/1' \
-H 'Content-Type: application/json' -d'
{
  "test_col1": "test_col1_data",
  "test_col2": "test_col2_data",
  "test_col3": "test_col3_data"
}
'

curl -u elastic:rG5bM465ha1x13Il2W74ZsXg -k -XPUT 'https://eck-elasticsearch-es-http.elastic-system.svc.cluster.local:9200/dev-apache-php/dev-apache-php/1' \
-H 'Content-Type: application/json' -d'
{
  "test_col1": "test_col1_data",
  "test_col2": "test_col2_data",
  "test_col3": "test_col3_data"
}
'

curl -u elastic:rG5bM465ha1x13Il2W74ZsXg https://eck-elasticsearch-es-http.elastic-system.svc.cluster.local:9200 -k


tadanobu@Yukiko-HP:Docker$ cd ~/Documents/Kubernetes/rancher-catalog; git add .; git commit . -m "elasticsearch更新"; git push -u origin main

http://192.168.0.130:5601/login?next=%2F
elastic
rG5bM465ha1x13Il2W74ZsXg

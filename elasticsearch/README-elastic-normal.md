# ＜１＞elasticsearch image build
tadanobu@Yukiko-HP:/$ docker login registry.gitlab.com
tadanobu@Yukiko-HP:/$ sudo /usr/sbin/service docker start; cd /mnt/c/k8s/rancher-catalog/mysql/Docker; docker build -t registry.gitlab.com/take-t14/rancher-catalog/mysql:latest .; docker push registry.gitlab.com/take-t14/rancher-catalog/mysql:latest
tadanobu@Yukiko-HP:Docker$ cd /mnt/c/k8s/rancher-catalog; git add .; git commit . -m "elasticsearch更新"; git push -u origin main

## １．Repositories
Name : elastic
URL : https://helm.elastic.co
Type : https

Name : lebenitza
URL : https://lebenitza.github.io/charts
Type : https


## ２．Appインストール
Name : elasticsearch
Namespace: common-systems
values:

esConfig:
  elasticsearch.yml: |
    xpack.security.enabled: false

nodeSelector: 
  kubernetes.io/hostname: 'rancher-master-01'

replicas: 1
minimumMasterNodes: 1

volumeClaimTemplate:
  storageClassName: hostpath

Name : kibana
Namespace: common-systems
values:

ingress:
  enabled: true


Name : elasticsearch-curator
Namespace: common-systems
values:

  action_file_yml: |-
    ---
    actions:
      1:
        filters:
        - filtertype: pattern
          kind: prefix
          value: idx-dev-apache-php-
        - filtertype: age
          source: name
          direction: older
          timestring: '%Y%m%d'
          unit: days
          unit_count: 1
          stats_result: min_value
          epoch:
          exclude: False
  config_yml: |-
    ---
    client:
      hosts:
        - elasticsearch-master.common-systems.svc.cluster.local
      use_ssl: False
      ssl_no_validate: True


## ３．pvデプロイ
[root@rancher-master-01 ~]# cd /mnt/c/k8s/rancher-catalog
[root@rancher-master-01 ~]# kubectl apply -f ./elasticsearch/pv-master-0.yaml -n common-systems
[root@rancher-master-01 ~]# kubectl apply -f ./elasticsearch/pv-master-1.yaml -n common-systems
[root@rancher-master-01 ~]# kubectl apply -f ./elasticsearch/pv-master-2.yaml -n common-systems

## ４．pvが作成されたノードで以下を実行
[root@rancher-master-01 ~]# chown -R 1000:1000 /var/lib/elasticsearch


## ９９．ElasticSearch登録テスト

### # elasticsearch-master
curl -XPUT 'http://elasticsearch-master.common-systems.svc.cluster.local:9200/dev-apache-php/dev-apache-php/1' \
-H 'Content-Type: application/json' -d'
{
  "test_col1": "test_col1_data",
  "test_col2": "test_col2_data",
  "test_col3": "test_col3_data"
}
'

tadanobu@Yukiko-HP:Docker$ cd ~/Documents/Kubernetes/rancher-catalog; git add .; git commit . -m "elasticsearch更新"; git push -u origin main

# Logging

## １．Outputs

apiVersion: logging.banzaicloud.io/v1beta1
kind: Output
metadata:
  name: dev-apache-php
  namespace: dev-apache-php
spec:
  elasticsearch:
    host: elasticsearch-master.common-systems.svc.cluster.local
    index_name: idx-dev-apache-php-%Y%m%d
    port: 9200
    scheme: http
    ssl_verify: false


## ２．Flows

apiVersion: logging.banzaicloud.io/v1beta1
kind: Flow
metadata:
  name: dev-apache-php
  namespace: dev-apache-php
spec:
  filters:
  - parser:
      parse:
        keep_time_key: true
        type: apache2
  - record_transformer:
      enable_ruby: true
      records:
      - '@timestamp': ${time.strftime('%Y-%m-%dT%H:%M:%S%z')}
  localOutputRefs:
  - dev-apache-php
  match:
  - select:
      labels:
        app: dev-apache-php


## ３．ClusterOutputs

apiVersion: logging.banzaicloud.io/v1beta1
kind: ClusterOutput
metadata:
  name: dev-apache-php-all-namespaces
  namespace: cattle-logging-system
spec:
  elasticsearch:
    host: elasticsearch-master.common-systems.svc.cluster.local
    index_name: idx-all-ns-dev-apache-php-%Y%m%d
    port: 9200
    scheme: http
    ssl_verify: false


## ４．ClusterFllows

apiVersion: logging.banzaicloud.io/v1beta1
kind: ClusterFlow
metadata:
  name: dev-apache-php
  namespace: cattle-logging-system
spec:
  filters:
  - parser:
      parse:
        keep_time_key: true
        type: apache2
  - record_transformer:
      enable_ruby: true
      records:
      - '@timestamp': ${time.strftime('%Y-%m-%dT%H:%M:%S%z')}
  globalOutputRefs:
  - dev-apache-php-all-namespaces
  match:
  - select:
      labels:
        app: dev-apache-php

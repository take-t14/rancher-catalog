#!/bin/bash

groupadd www-data
useradd www-data -g www-data
echo 'www-data:www-data' |chpasswd
useradd www-blog -g www-data
echo 'www-blog:www-blog' |chpasswd
echo 'root:ffff' |chpasswd

cp /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf_org

sed -i "s/    DirectoryIndex index.html/    DirectoryIndex index.php/" /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf
sed -i "s/#ServerName www.example.com:80/ServerName localhost:80/" /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf
sed -i "s/Listen 80/Listen 80\nListen 10080\nListen 9118/" /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf
sed -i "s/User apache/User www-data/" /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf
sed -i "s/Group apache/Group www-data/" /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf

echo "# クリックジャッキング対策" >> /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf
echo "Header always append X-Frame-Options SAMEORIGIN" \
    >> /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf

echo "# XSS対策" >> /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf
echo "Header set X-XSS-Protection '1; mode=block'" \
    >> /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf
echo "Header set X-Content-Type-Options nosniff" \
    >> /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf

echo "Include Includes/*.conf" \
    >> /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf

echo "PidFile '/run/httpd/httpd.pid'" \
    >> /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf

sed -i "s|LoadModule http2_module modules/mod_http2.so|#LoadModule http2_module modules/mod_http2.so|" /opt/rh/httpd24/root/etc/httpd/conf.modules.d/00-base.conf

mkdir -p /var/log/httpd24
mkdir -p /run/httpd
chmod -R 775 /var/log/httpd24
chmod -R 775 /run/httpd
chown -R www-data:www-data /var/log/httpd24
chown -R www-data:www-data /run/httpd

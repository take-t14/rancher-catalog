#!/bin/bash

lnonly=$1
shift

if [[ 0 = $lnonly ]]; then
    /usr/local/bin/entrypoint-setup.sh
fi

ln -s /var/www/html/laravel-ddd-sample /home/laravel-ddd-sample

exec "${@}"
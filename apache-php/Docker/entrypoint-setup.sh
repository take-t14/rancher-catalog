#!/bin/bash

function deploy() {
    GIT_SSH_URL="git@github.com:take-t14/laravel-ddd-sample.git"

    if [[ -d /var/www/html/laravel-ddd-sample ]]; then
        return
    fi
    mkdir -p /var/www/html/laravel-ddd-sample
    chown -R www-data:www-data /var/www/html/laravel-ddd-sample
    chmod -R 755 /var/www/html/laravel-ddd-sample

    su - www-data -c "ssh-keyscan -t rsa github.com >> ~/.ssh/known_hosts; \
cd /var/www/html/laravel-ddd-sample; \
git clone ${GIT_SSH_URL} .; \
git checkout -b ${BRANCH} origin/${BRANCH}; \
git pull; \
git checkout .; \
mkdir /var/www/html/laravel-ddd-sample/log; \
"

    if [ "latest" != "${TAG}" ]; then
        su - www-data -c "cd /var/www/html/laravel-ddd-sample; \
git checkout ${TAG}; \
"
    fi

    su - www-data -c "cd /var/www/html/laravel-ddd-sample; \
/opt/remi/php83/root/usr/bin/php /usr/local/bin/composer install; \
npm install; \
/opt/remi/php83/root/usr/bin/php artisan --env=${APP_ENV} cache:clear; \
/opt/remi/php83/root/usr/bin/php artisan --env=${APP_ENV} config:clear; \
/opt/remi/php83/root/usr/bin/php artisan --env=${APP_ENV} route:clear; \
/opt/remi/php83/root/usr/bin/php artisan --env=${APP_ENV} view:clear; \
/opt/remi/php83/root/usr/bin/php /usr/local/bin/composer dump-autoload; \
/opt/remi/php83/root/usr/bin/php artisan --env=${APP_ENV} clear-compiled; \
/opt/remi/php83/root/usr/bin/php artisan --env=${APP_ENV} optimize; \
/opt/remi/php83/root/usr/bin/php artisan --env=${APP_ENV} config:cache; \
npm run ${APP_ENV}; \
"
}

LOCKFILE=/var/www/html/flock.lock
exec {fd}> "${LOCKFILE}" # ファイルディスクリプタ作成
{
    flock -w 900 -x ${fd} || {
        echo "TIMEOUT"
        exit 1;
    }

    # 排他したい処理をここに書く
    deploy

    flock -u ${fd}
}


#!/bin/bash

rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
 && rpm -Uvh https://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum -y update && yum -y install php83 php83-php php83-php-opcache php83-php-bcmath php83-php-cli \
  php83-php-common php83-php-gd php83-php-intl php83-php-json \
  php83-php-mbstring php83-php-pdo php83-php-pdo-dblib \
  php83-php-devel php83-php-pear php83-php-smarty php83-php-pecl-mcrypt \
  php83-php-xmlrpc php83-php-xml php83-php-mysql php83-php-soap \
  php83-php-fpm php83-php-pdo php83-php-pgsql php83-php-pecl-zip \
  php83-php-pecl-mongodb php83-php-pecl-xdebug3 php83-php-pecl-yaml \
  php83-php-pecl-memcache php83-php-pecl-memcached \
  ssmtp mailutils gcc-c++ make && yum clean all
cp /etc/opt/remi/php83/php.ini /etc/opt/remi/php83/php.ini_org
cp /etc/opt/remi/php83/php-fpm.conf /etc/opt/remi/php83/php-fpm.conf_org
cp -RT /etc/opt/remi/php83/php-fpm.d /etc/opt/remi/php83/php-fpm.d_org
ln -s /opt/remi/php83/root/usr/bin/php /opt/remi/php83/root/usr/bin/php83
ln -s /opt/remi/php83/root/usr/sbin/php-fpm /opt/remi/php83/root/usr/sbin/php83-fpm
#!/bin/bash

echo "`date '+%Y-%m-%d %H:%M:%S'` [START] docker-init.sh"

if [ -e /var/lib/mysql ]; then rm -rf /var/lib/mysql; fi

if [[ 1 == `ls -l /mnt/mysql-pvc | wc -l` ]]; then chown -R mysql:mysql /mnt/mysql-pvc; fi

if [ ! -e /mnt/mysql-pvc/repl-${HOSTNAME##*-} ]; then
	mkdir /mnt/mysql-pvc/repl-${HOSTNAME##*-}
	chown -R mysql:mysql /mnt/mysql-pvc/repl-${HOSTNAME##*-}
fi
ln -s /mnt/mysql-pvc/repl-${HOSTNAME##*-} /var/lib/mysql

if [[ "master" != "${MYSQL_REPLICATION_MODE}" ]]; then
	mysqldump \
		-h ${MYSQL_MASTER_HOST} \
		--user=root \
		--password=${MYSQL_ROOT_PASSWORD} \
		--master-data=2 \
		--hex-blob \
		--default-character-set=utf8 \
		--all-databases \
		--single-transaction \
		--triggers \
		--routines \
		--events \
		> /usr/local/local_script/sql/slave/2-slave-init.sql
	sed -i "s/^WARNING: --master-data is deprecated and will be removed in a future version\. Use --source-data instead\.$//" /usr/local/local_script/sql/slave/2-slave-init.sql
	chown -R mysql:mysql /usr/local/local_script/sql/slave/2-slave-init.sql
fi

echo "`date '+%Y-%m-%d %H:%M:%S'` [END] docker-init.sh"

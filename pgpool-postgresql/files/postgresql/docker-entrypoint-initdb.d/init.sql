SET password_encryption = 'md5';
CREATE ROLE pgpool WITH LOGIN;
CREATE ROLE repl WITH REPLICATION LOGIN;
ALTER USER postgres WITH PASSWORD 'ffff';
ALTER USER pgpool WITH PASSWORD 'ffff';
ALTER USER repl WITH PASSWORD 'ffff';
GRANT pg_monitor TO pgpool;
#!/bin/bash

echo "Postgresql wal cron..."

. /usr/local/local_script/pre-init-postgresql.sh

echo "POSTGRESQL_DATA_DIR : $POSTGRESQL_DATA_DIR"
echo "POSTGRESQL_MASTER_HOST : $POSTGRESQL_MASTER_HOST"
/usr/pgsql-14/bin/pg_controldata "$POSTGRESQL_DATA_DIR" | grep "Latest checkpoint's REDO WAL file:" | awk -F ": +" '{print $2}' | ssh postgres@$POSTGRESQL_MASTER_HOST "cat > ~/walfilename.dat"


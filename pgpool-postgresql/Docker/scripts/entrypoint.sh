#!/bin/bash

# Start Pgpool-II and Postgresql
echo "Starting Pgpool-II and Postgresql..."

/usr/pgsql-14/bin/postgres -D ${POSTGRESQL_DATA_DIR} -c config_file=${POSTGRESQL_DATA_DIR}/postgresql.conf

#!/bin/bash

echo "Postgresql init db..."

. /usr/local/local_script/pre-init-postgresql.sh

# if [[ "$*" = *"/usr/local/local_script/run.sh"* ]]; then
if [ ! -e ${POSTGRESQL_DATA_DIR} ]; then
    info "** Starting PostgreSQL setup **"
    cat /pgpool/conf.d/.pcppass > /var/lib/pgsql/.pcppass
    chmod 600 /var/lib/pgsql/.pcppass
    chown -R postgres:postgres /var/lib/pgsql/.pcppass
    cat /postgresql/conf.d/.pgpass > /var/lib/pgsql/.pgpass
    chmod 600 /var/lib/pgsql/.pgpass
    chown -R postgres:postgres /var/lib/pgsql/.pgpass
    . /usr/local/local_script/setup.sh
    touch "$POSTGRESQL_TMP_DIR"/.initialized
    info "** PostgreSQL setup finished! **"]
else
    info "** Starting PostgreSQL Config setup **"
    postgresql_custom_init_conf
    # Allow running custom initialization scripts
    postgresql_custom_init_scripts
    postgresql_custom_init_scripts ${POSTGRESQL_VOLUME_SQL_DIR} ".user_sql_initialized"
fi

if [ ! -e /var/lib/pgsql/.ssh ]; then
    mkdir /var/lib/pgsql/.ssh
fi
cat /mnt/ssh-key/id_rsa > /var/lib/pgsql/.ssh/id_rsa
cat /mnt/ssh-key/id_rsa.pub > /var/lib/pgsql/.ssh/id_rsa.pub
echo -e "Host *\n" > /var/lib/pgsql/.ssh/config
echo -e "    StrictHostKeyChecking no\n" >> /var/lib/pgsql/.ssh/config
chmod 700 /var/lib/pgsql/.ssh
chmod 600 /var/lib/pgsql/.ssh/*
cat /postgresql/conf.d/recovery_1st_stage > ${POSTGRESQL_BASE_DIR}/recovery_1st_stage
cat /postgresql/conf.d/pgpool_remote_start > ${POSTGRESQL_BASE_DIR}/pgpool_remote_start
# psql template1 -c "CREATE EXTENSION pgpool_recovery"
cat /pgpool/conf.d/.pgpoolkey > /var/lib/pgsql/.pgpoolkey
chmod 600 /var/lib/pgsql/.pgpoolkey
cat /pgpool/conf.d/pool_passwd > /var/lib/pgsql/pool_passwd
cat /pgpool/conf.d/pgpool > /etc/sysconfig/pgpool

chown -R postgres:postgres ${POSTGRESQL_BASE_DIR}
chown -R postgres:postgres /var/lib/pgsql/

sed -i "s|\${POSTGRESQL_DATA_DIR}|${POSTGRESQL_DATA_DIR}|g" /usr/local/local_script/entrypoint.sh 

#!/bin/bash

# pgpool init db
echo "pgpool init db..."

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

# Load libraries
. /usr/local/local_script/libpostgresql.sh

# Load PostgreSQL environment variables
. /usr/local/local_script/postgresql-env.sh

pgpool_custom_init_conf
echo 1 > /etc/pgpool-II/pgpool_node_id
cat /pgpool/conf.d/failover.sh > /etc/pgpool-II/failover.sh
cat /pgpool/conf.d/follow_primary.sh > /etc/pgpool-II/follow_primary.sh
cat /pgpool/conf.d/pcp.conf > /etc/pgpool-II/pcp.conf
cat /pgpool/conf.d/escalation.sh > /etc/pgpool-II/escalation.sh

if [ ! -e /root/.ssh ]; then
    mkdir /root/.ssh
fi
cp /mnt/ssh-key/id_rsa /root/.ssh/id_rsa
cp /mnt/ssh-key/id_rsa.pub /root/.ssh/id_rsa.pub
chmod 700 /root/.ssh
chmod 600 /root/.ssh/*
chown -R postgres:postgres /etc/pgpool-II

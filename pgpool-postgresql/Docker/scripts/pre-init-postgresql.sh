#!/bin/bash

# debug
# tail -f /dev/null 

# Postgresql init db
echo "Postgresql pre init db..."
# su postgres -c "/usr/pgsql-14/bin/initdb --encoding=UTF-8 --locale='C' -D ${POSTGRESQL_BASE_DIR}"

# shellcheck disable=SC1091

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

# Load libraries
. /usr/local/local_script/libbitnami.sh
. /usr/local/local_script/libpostgresql.sh

# Load PostgreSQL environment variables
. /usr/local/local_script/postgresql-env.sh

print_welcome_page

# Enable the nss_wrapper settings
postgresql_enable_nss_wrapper

if [ ! -e ${POSTGRESQL_VOLUME_REPL_DIR} ]; then
    mkdir -p ${POSTGRESQL_VOLUME_REPL_DIR}
fi
if [ ! -e ${POSTGRESQL_VOLUME_SQL_DIR} ]; then
    mkdir -p ${POSTGRESQL_VOLUME_SQL_DIR}
fi
rm -rf ${POSTGRESQL_VOLUME_DIR}
ln -s ${POSTGRESQL_VOLUME_REPL_DIR} ${POSTGRESQL_VOLUME_DIR}
chown -R postgres:postgres ${POSTGRESQL_VOLUME_DIR}/
su postgres -c "mkdir -p ${POSTGRESQL_VOLUME_DIR}/archivedir"

if [ ! -e /root/.ssh ]; then
    mkdir /root/.ssh
fi
cp /mnt/ssh-key/id_rsa /root/.ssh/id_rsa
cp /mnt/ssh-key/id_rsa.pub /root/.ssh/id_rsa.pub
echo  -e "Host *\n" > /root/.ssh/config
echo  -e "    StrictHostKeyChecking no\n" >> /root/.ssh/config
chmod 700 /root/.ssh
chmod 600 /root/.ssh/*

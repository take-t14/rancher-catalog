# Run Pgpool-II on Kubernetes

This repository contains Dockerfile and <code>YAML</code> files that can be used to deploy [Pgpool-II](https://pgpool.net "Pgpool-II") and PostgreSQL Streaming Replication with [KubeDB](https://kubedb.com/ "KubeDB") on Kubernetes.

# Requirements
- Make sure you have a Kubernetes cluster, and the kubectl is installed.
- Kebernetes 1.15 or older is required.

# Usage

See [docs](docs/index.md).


# pgpool image build
tadanobu@Yukiko-HP:/$ docker login registry.gitlab.com
tadanobu@Yukiko-HP:/$ sudo /usr/sbin/service docker start; cd /mnt/c/k8s/rancher-catalog/pgpool-postgresql/Docker; docker build -t registry.gitlab.com/take-t14/rancher-catalog/pgpool:latest .; docker push registry.gitlab.com/take-t14/rancher-catalog/pgpool:latest
tadanobu@Yukiko-HP:Docker$ cd /mnt/c/k8s/rancher-catalog; git add .; git commit . -m "pgpool更新"; git push -u origin main



## 起動＆コンテナ接続
docker container run --name pgpool -it --entrypoint "/bin/bash" registry.gitlab.com/take-t14/rancher-catalog/pgpool:latest

## 起動済みコンテナへ接続
docker exec -it pgpool /bin/bash

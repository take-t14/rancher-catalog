#!/bin/bash

lnonly=$1
shift

# pvcの共有ボリュームへのシンボリックリンク設定（排他）
LOCKFILE=/mnt/pvc/deploy.lock
exec {fd}> "${LOCKFILE}" # ファイルディスクリプタ作成
{
    flock -w ${APP_INIT_TIMEOUT} -x ${fd} || {
        echo "[ERROR] deploy timeout." 1>&2
        exit 1;
    }
    
    # 排他したい処理をここに書く
    /usr/local/bin/entrypoint-setup.sh $lnonly 2>&1 | tee -a /mnt/pvc/entrypoint.log

    flock -u ${fd}
}

git config --global user.name ${GIT_USER}
git config --global user.email ${GIT_MAIL}

if [[ 1 = $lnonly ]]; then
    [[ ! -e /mnt/pvc/.subversion ]] && mkdir -p /mnt/pvc/.subversion
    if [[ ! -e /mnt/pvc/.subversion/auth ]]; then
        if [[ -e /home/www-data/.subversion/auth ]]; then
            mv -a /home/www-data/.subversion/auth /mnt/pvc/.subversion/
        else
            mkdir -p /mnt/pvc/.subversion/auth
        fi
        chown -R www-data:www-data /mnt/pvc/.subversion
    fi
fi
ln -s /mnt/pvc/.subversion/auth /home/www-data/.subversion/auth
chown -h www-data:www-data /home/www-data/.subversion/auth

exec "${@}"
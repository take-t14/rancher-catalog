#!/bin/bash

# normal updates
dnf -y update

# httpd
dnf -y install httpd mod_proxy_html mod_session mod_ssl && \
  dnf clean all

mkdir -p /etc/httpd/Includes
mkdir -p /etc/httpd/ssl
mv /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf_org
mv /etc/httpd/conf.d/ssl.conf /etc/httpd/conf.d/ssl.conf_org

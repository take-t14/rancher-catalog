#!/bin/bash

#groupadd www-data -g 1000
#useradd www-data -u 1000 -g www-data
groupadd www-data -g 1000
useradd www-data -u 501 -g www-data -G games

echo "# クリックジャッキング対策" >> /etc/httpd/conf/httpd.conf
echo "Header always append X-Frame-Options SAMEORIGIN" \
    >> /etc/httpd/conf/httpd.conf

echo "# XSS対策" >> /etc/httpd/conf/httpd.conf
echo "Header set X-XSS-Protection '1; mode=block'" \
    >> /etc/httpd/conf/httpd.conf
echo "Header set X-Content-Type-Options nosniff" \
    >> /etc/httpd/conf/httpd.conf

echo "Include Includes/*.conf" \
    >> /etc/httpd/conf/httpd.conf

echo "PidFile '/run/httpd/httpd.pid'" \
    >> /etc/httpd/conf/httpd.conf

sed -i "s|LoadModule http2_module modules/mod_http2.so|#LoadModule http2_module modules/mod_http2.so|" /etc/httpd/conf.modules.d/00-base.conf

mkdir -p /var/log/httpd24
mkdir -p /run/httpd
chmod -R 775 /var/log/httpd24
chmod -R 775 /run/httpd
chown -R www-data:www-data /var/log/httpd24
chown -R www-data:www-data /run/httpd

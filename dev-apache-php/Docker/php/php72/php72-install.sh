#!/bin/bash

rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm \
 && rpm -Uvh https://rpms.remirepo.net/enterprise/remi-release-8.5.rpm
dnf -y update && dnf install -y dnf-plugins-core && dnf config-manager --set-enabled powertools && \
  dnf -y install php72 php72-php php72-php-opcache php72-php-bcmath php72-php-cli \
  php72-php-common php72-php-gd php72-php-intl php72-php-json php72-php-mbstring php72-php-pdo php72-php-pdo-dblib \
  php72-php-devel php72-php-pear php72-php-pecl-mcrypt php72-php-xmlrpc php72-php-xml php72-php-mysql php72-php-soap \
  php72-php-fpm php72-php-pdo php72-php-pgsql php72-php-pecl-zip php72-php-pecl-mongodb php72-php-pecl-xdebug php72-php-pecl-yaml \
  php72-php-pecl-memcache php72-php-pecl-memcached php72-php-pecl-apcu php72-php-mysqlnd php72-php-pecl-mysql php72-php-process \
  gcc-c++ make && dnf clean all
cp /etc/opt/remi/php72/php.ini /etc/opt/remi/php72/php.ini_org
cp /etc/opt/remi/php72/php-fpm.conf /etc/opt/remi/php72/php-fpm.conf_org
cp -RT /etc/opt/remi/php72/php-fpm.d /etc/opt/remi/php72/php-fpm.d_org
ln -s /opt/remi/php72/root/usr/bin/php /opt/remi/php72/root/usr/bin/php72
ln -s /opt/remi/php72/root/usr/sbin/php-fpm /opt/remi/php72/root/usr/sbin/php72-fpm

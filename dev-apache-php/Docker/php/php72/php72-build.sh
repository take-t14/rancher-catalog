#!/bin/bash

sed -i "s/;date.timezone =/date.timezone = Asia\/Tokyo/" /etc/opt/remi/php72/php.ini

# extention module
mv /etc/opt/remi/php72/php.d /etc/opt/remi/php72/php.d_back
mkdir /etc/opt/remi/php72/php.d
cp -a /etc/opt/remi/php72/php.d_back/*opcache*.ini /etc/opt/remi/php72/php.d/
cp -a /etc/opt/remi/php72/php.d_back/*xdebug*.ini /etc/opt/remi/php72/php.d/
cp -a /etc/opt/remi/php72/php.d_back/*memcache*.ini /etc/opt/remi/php72/php.d/
cp -a /etc/opt/remi/php72/php.d_back/*igbinary*.ini /etc/opt/remi/php72/php.d/
cp -a /etc/opt/remi/php72/php.d_back/*msgpack*.ini /etc/opt/remi/php72/php.d/
cp -a /etc/opt/remi/php72/php.d_back/opcache-default.blacklist /etc/opt/remi/php72/php.d/

cd /tmp/
curl -sS https://getcomposer.org/installer | /opt/remi/php72/root/usr/bin/php -- --version=1.10.15
[[ ! -d /usr/local/bin/php72 ]] && mkdir -p /usr/local/bin/php72
mv -i composer.phar /usr/local/bin/php72/composer
echo "alias composer72='/opt/remi/php72/root/usr/bin/php /usr/local/bin/php72/composer'" >> /etc/bashrc

chown root:www-data /opt/remi/php72/root/usr/sbin/php-fpm && \
  chmod 775 /opt/remi/php72/root/usr/sbin/php-fpm && \
  mkdir -p /var/opt/remi/php72 && \
  touch /var/opt/remi/php72/stdout && \
  chown -R root:www-data /var/opt/remi/php72 && \
  chmod -R 775 /var/opt/remi/php72 && \
  mkdir -p /var/log/php72 && \
  chown -R www-data:www-data /var/log/php72
  #mkdir -p /var/opt/remi/php72/stdout && \
  #chown root:www-data /var/opt/remi/php72/stdout && \
  #chmod 660 /var/opt/remi/php72/stdout && \
  #chown root:www-data /var/opt/remi/php72/run/php-fpm && \
  #chmod 775 /var/opt/remi/php72/run/php-fpm

touch /var/log/php72/xdebug.log
chmod 777 /var/log/php72/xdebug.log

# deployer
cd /tmp/
curl -LO https://deployer.org/deployer.phar
mv deployer.phar /usr/local/bin/dep
chmod +x /usr/local/bin/dep

# Blackfile
# yum -y install pygpgme
# yum -y install wget
# wget -O - "http://packages.blackfire.io/fedora/blackfire.repo" | tee /etc/yum.repos.d/blackfire.repo
# yum -y install blackfire-agent; exit 0
# blackfire-agent --register --server-id=36bee2c2-b075-4221-baae-589751143491 --server-token=98481d46914bc4eb2e660db9549673bf27690ef0a8abbe7638cd9781acdd2d1e
# /etc/init.d/blackfire-agent restart
# yum -y install blackfire-php
# blackfire config --client-id=e999c7d3-31d3-43b2-880f-14ae2dcc18a6 --client-token=0326695de8e4e1e9f6bdf9f629f61abe37e6931ff0d409e34dac9367e2acdf6e
# yum clean all


#!/bin/bash
echo `echo $POD_NAME` `date` "[S]entrypoint-setup.sh =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>"

lnonly=0
if [[ $# -ge 1 ]] && [[ $1 != 0 ]]; then
    lnonly=1
fi

# gitの鍵設定
cat /mnt/git-secret/id_rsa > /root/.ssh/id_rsa
cat /mnt/git-secret/id_rsa.pub > /root/.ssh/id_rsa.pub
cat /mnt/git-secret/id_rsa > /home/www-data/.ssh/id_rsa
cat /mnt/git-secret/id_rsa.pub > /home/www-data/.ssh/id_rsa.pub
cat /root/.ssh/id_rsa.pub > /root/.ssh/authorized_keys
cat /root/.ssh/id_rsa.pub > /home/www-data/.ssh/authorized_keys
ssh-keyscan -t rsa github.com > /root/.ssh/known_hosts
su - www-data -c "ssh-keyscan -t rsa github.com > ~/.ssh/known_hosts"

function deploy_apache() {
    echo `echo $POD_NAME` "[S]deploy_apache f=${1} lnonly=${6}"
    f=$1
    config=$2
    apacheConf=$3
    lnonly=$6
    #if [[ -e "${apacheConf}" ]] || [[ 1 = $lnonly ]]; then
    #    echo `echo $POD_NAME` "[E]deploy_apache f=${1} lnonly=${6}"
    #    return
    #fi
    hostName=$4
    phpIni=$5
    if [[ ! -e "${phpIni}" ]]; then
        \cp -f /usr/local/php/template-php.ini /usr/local/php/template-php_tmp.ini
        sed -i "s/\${hostName}/${hostName}/" /usr/local/php/template-php_tmp.ini
        phpIni=/usr/local/php/template-php_tmp.ini
    fi
    phpVersion=72
    tmpPhpVersion=`cat "${config}" | grep 'phpVersion=' | sed -e "s/^phpVersion=\([0-9]\{2\}\)$/\1/g"`
    if [[ "$tmpPhpVersion" =~ ^[0-9]+$ ]]; then
        phpVersion="${tmpPhpVersion}"
    fi
    serverName=`cat "${config}" | grep 'serverName=' | sed -e "s/^serverName=\(.*\)$/\1/g"`
    cat "${f}" > "${apacheConf}"
    sed -i "s/\${serverName}/${serverName}/g" "${apacheConf}"
    sed -i "s/\${phpVersion}/${phpVersion}/g" "${apacheConf}"
    echo -e "\n\n" >> /etc/opt/remi/php56/php.ini
    echo -e "\n\n" >> /etc/opt/remi/php72/php.ini
    echo -e "\n\n" >> /etc/opt/remi/php83/php.ini
    cat "${phpIni}" >> /etc/opt/remi/php56/php.ini
    cat "${phpIni}" >> /etc/opt/remi/php72/php.ini
    cat "${phpIni}" >> /etc/opt/remi/php83/php.ini
    sed -i "s/\[HOST=${hostName}\]/[HOST=${serverName}]/" /etc/opt/remi/php56/php.ini
    sed -i "s/\[HOST=${hostName}\]/[HOST=${serverName}]/" /etc/opt/remi/php72/php.ini
    sed -i "s/\[HOST=${hostName}\]/[HOST=${serverName}]/" /etc/opt/remi/php83/php.ini
    echo `echo $POD_NAME` "[E]deploy_apache f=${1} lnonly=${6}"
}

function checkout() {
    echo `echo $POD_NAME` "[S]checkout hostName=${2} lnonly=${4} initFlg=${5}"
    f=$1
    hostName=$2
    config=$3
    lnonly=$4
    initFlg=$5
    hostPath="/mnt/pvc/${hostName}"
    if [[ "dasales" -eq "${hostName}" ]]; then
        hostTrunkPath="${hostPath}"
    else
        hostTrunkPath="${hostPath}/trunk"
    fi
    git=`cat "${config}" | grep 'git=' | sed -e "s/^git=\(.*\)$/\1/g"`
    branch=`cat "${config}" | grep 'branch=' | sed -e "s/^branch=\(.*\)$/\1/g"`
    tag=`cat "${config}" | grep 'tag=' | sed -e "s/^tag=\(.*\)$/\1/g"`
    svn=`cat "${config}" | grep 'svn=' | sed -e "s/^svn=\(.*\)$/\1/g"`
    if [[ 1 = $initFlg ]]; then
        echo `echo $POD_NAME` "checkout git:${git}"
        echo `echo $POD_NAME` "checkout svn:${svn}"
        if [[ ! -z "${git}" ]] || [[ ! -z "${svn}" ]]; then
            mkdir -p "${hostPath}"
            chown -R www-data:www-data "${hostPath}"
            chmod -R 755 "${hostPath}"
        fi
        if [[ ! -z "${git}" ]]; then
            su - www-data -c "cd ${hostPath}; \
git clone ${git} .; \
[[ ! -d '${hostPath}/log' ]] && mkdir ${hostPath}/log; \
mkdir ${hostPath}/.vscode; \
cp /usr/local/vscode/launch.json ${hostPath}/.vscode/; \
sed -i 's|\${hostPath}|${hostPath}|g' ${hostPath}/.vscode/launch.json; \
"
            if [[ ! -z "${branch}" ]]; then
                su - www-data -c "cd ${hostPath}; \
git checkout -b ${branch} origin/${branch}; \
"
            fi
            if [ "latest" != "${tag}" ]; then
                su - www-data -c "cd ${hostPath}; \
git checkout ${tag}; \
"
            fi
        elif [[ ! -z "${svn}" ]]; then
#           maxRetry=5
#           tryCount=0
#           until [ $tryCount -ge $maxRetry ]
#           do
                su - www-data -c "cd ${hostPath}; \
svn co ${svn} . --username ${SVN_USER} --password ${SVN_PASS}; \
[[ ! -d '${hostTrunkPath}/log' ]] && mkdir ${hostTrunkPath}/log; \
mkdir ${hostTrunkPath}/.vscode; \
cp /usr/local/vscode/launch.json ${hostTrunkPath}/.vscode/; \
sed -i 's|\${hostPath}|${hostTrunkPath}|g' ${hostTrunkPath}/.vscode/launch.json; \
"
#               if [[ ! -d "${hostTrunkPath}" ]] || [[ ! -d "${hostTrunkPath}/log" ]]; then
#                   tryCount=$[$tryCount+1]
#                   sleep $[ ( $RANDOM % 15 )  + 1 ]s
#               else
#                   tryCount=$maxRetry
#               fi
#           done
        fi
    fi
    if [[ ! -z "${git}" ]]; then
        ln -s "${hostPath}" "/home/${hostName}"
    elif [[ ! -z "${svn}" ]]; then
        ln -s "${hostTrunkPath}" "/home/${hostName}"
    fi
    echo `echo $POD_NAME` "[E]checkout hostName=${2} lnonly=${4} initFlg=${5}"
}

function entrypoint() {
    echo `echo $POD_NAME` "[S]entrypoint hostName=${1} lnonly=${2} initFlg=${3}"
    hostName=$1
    lnonly=$2
    initFlg=$3
    entrypointSh="/init-config/entrypoint/${hostName}.sh"
    if [[ -e "${entrypointSh}" ]]; then
        . "${entrypointSh}" $initFlg
    fi
    echo `echo $POD_NAME` "[E]entrypoint hostName=${1} lnonly=${2} initFlg=${3}"
}

function deploy() {
    echo `echo $POD_NAME` "[S]deploy f=${1} lnonly=${2}"
    f=$1
    lnonly=$2
    hostName=`basename $f .conf`
    apacheConf="/etc/httpd/Includes/${hostName}.conf"
    phpIni="/init-config/apache-conf.d/${hostName}-php.ini"
    config="/init-config/apache-conf.d/${hostName}.config"
    hostPath="/mnt/pvc/${hostName}"
    initFlg=0
    if [[ ! -d "${hostPath}" ]] && [[ 0 = $lnonly ]]; then
        initFlg=1
    fi
    if cat "${config}" | grep 'enabled=true' > /dev/null; then
        # apache設定ファイル配備
        deploy_apache "${f}" "${config}" "${apacheConf}" "${hostName}" "${phpIni}" $lnonly

        # checkout
        checkout "${f}" "${hostName}" "${config}" $lnonly $initFlg

        # entrypoint実行
        entrypoint "${hostName}" $lnonly $initFlg
    fi
    echo `echo $POD_NAME` "[E]deploy f=${1} lnonly=${2}"
}

find "/init-config/apache-conf.d/" -type f -regex ".*\.\(conf\)" | sort | while read -r f; do
    case "$f" in
    *.conf) echo `echo $POD_NAME` "[S]find ${f}"; deploy "$f" $lnonly; echo `echo $POD_NAME` "[E]find ${f}";;
    *) echo "Ignoring $f" ;;
    esac
done

echo `echo $POD_NAME` `date` "[E]entrypoint-setup.sh <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<="

#!/bin/bash

APP_HOST=`find "/init-config/apache-conf.d/" -type f -regex ".*\.\(conf\)" | grep 'laravel-ddd-sample'`
APP_HOST=`basename $APP_HOST .conf`
config="/init-config/apache-conf.d/${APP_HOST}.config"
serverName=`cat "${config}" | grep 'serverName=' | sed -e "s/^serverName=\(.*\)$/\1/g"`
initFlg=$1
phpVersion=72
tmpPhpVersion=`cat "${config}" | grep 'phpVersion=' | sed -e "s/^phpVersion=\([0-9]\{2\}\)$/\1/g"`
if [[ "$tmpPhpVersion" =~ ^[0-9]+$ ]]; then
    phpVersion="${tmpPhpVersion}"
fi
if [[ "$phpVersion" != "83" ]]; then
    exit 0
fi

if [[ 1 = $initFlg ]]; then
    su - www-data -c "cd /home/${APP_HOST}; \
[[ ! -e ./log ]] && mkdir ./log; \
/opt/remi/php${phpVersion}/root/usr/bin/php${phpVersion} /usr/local/bin/composer install; \
npm install; \
"
fi

su - www-data -c "cd /home/${APP_HOST}; \
/opt/remi/php${phpVersion}/root/usr/bin/php${phpVersion} artisan --env=${APP_ENV} cache:clear; \
/opt/remi/php${phpVersion}/root/usr/bin/php${phpVersion} artisan --env=${APP_ENV} config:clear; \
/opt/remi/php${phpVersion}/root/usr/bin/php${phpVersion} artisan --env=${APP_ENV} route:clear; \
/opt/remi/php${phpVersion}/root/usr/bin/php${phpVersion} artisan --env=${APP_ENV} view:clear; \
/opt/remi/php${phpVersion}/root/usr/bin/php${phpVersion} /usr/local/bin/composer dump-autoload; \
/opt/remi/php${phpVersion}/root/usr/bin/php${phpVersion} artisan --env=${APP_ENV} clear-compiled; \
/opt/remi/php${phpVersion}/root/usr/bin/php${phpVersion} artisan --env=${APP_ENV} optimize; \
/opt/remi/php${phpVersion}/root/usr/bin/php${phpVersion} artisan --env=${APP_ENV} config:cache; \
npm run ${APP_ENV}; \
"
